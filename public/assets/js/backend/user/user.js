define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: "user/user/index" + location.search,
                    add_url: 'user/user/add',
                    edit_url: 'user/user/edit',
                    del_url: 'user/user/del',
                    multi_url: 'user/user/multi',
                    table: 'user',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'user.id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id'), sortable: true},
                        {field: 'jointime', title: '注册时间', formatter: Table.api.formatter.datetime, operate: 'RANGE', addclass: 'datetimerange', sortable: true},
                        {field: 'username', title:'姓名', operate: 'LIKE'},
                        {field: 'mobile', title: __('Mobile'), operate: 'LIKE'},
                        {field: 'idcard', title: '身份证号', operate: 'LIKE'},
                        {field: 'cardno', title: '医疗卡号', operate: 'LIKE'},
                        {
                            field: 'money',
                            title: '余额',
                            formatter: Controller.api.formatter.subnode
                        },
                        {field: 'fromname', title: '推荐人姓名'},
                        {field: 'frommobile', title: '推荐人手机号'},
                        {field: 'logintime', title: __('Logintime'), formatter: Table.api.formatter.datetime, operate: 'RANGE', addclass: 'datetimerange', sortable: true},
                        {field: 'status', title: __('Status'), formatter: Table.api.formatter.status, searchList: {normal: __('Normal'), hidden: __('Hidden')}},
                        {
                            field: 'fromid',
                            title: '查看推荐关系',
                            formatter: Controller.api.formatter.users
                        },
                        {field: 'operate', title: __('Operate'), table: table,    buttons: [
                                {
                                    name: 'buyCard',
                                    title: '办卡',
                                    text: '办卡',
                                    classname: 'btn btn-xs btn-primary btn-dialog btn-bk',
                                    icon: 'fa fa-address-card-o',
                                    url: 'user/user/operate?do=buyCard',
                                    callback: function (data) {
                                        Layer.alert("接收到回传数据：" + JSON.stringify(data), {title: "回传数据"});
                                    }
                                },
                                {
                                    name: 'incMoney',
                                    title: '充值',
                                    text: '充值',
                                    classname: 'btn btn-xs btn-primary btn-dialog',
                                    icon: 'fa fa-money',
                                    url: 'user/user/operate?do=incMoney',
                                    callback: function (data) {
                                        Layer.alert("接收到回传数据：" + JSON.stringify(data), {title: "回传数据"});
                                    }
                                },
                            ],
                            events: Table.api.events.operate, formatter: Table.api.formatter.operate,
                            formatter: function (value, row, index) { //隐藏自定义按钮
                                var that = $.extend({}, this);
                                var table = $(that.table).clone(true);
                                if(row.cardno) {
                                    $(table).data("operate-buyCard", null);  //隐藏按钮操作 video为自定义按钮的name
                                    // $(table).data("operate-fail", null);  //隐藏按钮操作 video为自定义按钮的name
                                }
                                that.table = table;
                                return Table.api.formatter.operate.call(that, value, row, index);
                            },}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {
                subnode: function (value, row) {
                    return '<a onclick="ticketList(' + row.id + ')">'+row.money+'</a>';
                },
                users: function (value, row) {
                    return '<a onclick="UserList(' + row.id + ')" class="fa fa-users"></a>';
                }
            },
            events: {
                'click .btn-buycard': function (e, value, row, index) {
                    buyCard(row.id);
                }
            }
        }
    };
    return Controller;



});
function ticketList(id) {
    var url = 'user_ticket?uid='+id;//弹出窗口 add.html页面的（fastadmin封装layer模态框将以iframe的方式将add输出到index页面的模态框里）
    Fast.api.open(url,'优惠券明细');
}
function UserList(id) {
    var url = 'user/user?uid='+id;//弹出窗口 add.html页面的（fastadmin封装layer模态框将以iframe的方式将add输出到index页面的模态框里）
    Fast.api.open(url,'推荐关系');
}
